/*  ********************** CARGO EL BODY  **********************  */
window.addEventListener("load", () => {

    setInterval(() => {
        var d = new Date;
        var h = d.getHours();
        var m = d.getMinutes();
        var s = d.getSeconds();

        var h1=Math.floor(h/10);
        var h0=h%10;

        var m1=Math.floor(m/10);
        var m0=m%10;

        var s1=Math.floor(s/10);
        var s0=s%10;

        document.querySelector("#h1").innerHTML = h1;
        document.querySelector("#h0").innerHTML = h0;
        document.querySelector("#m1").innerHTML = m1;
        document.querySelector("#m0").innerHTML = m0;
        document.querySelector("#s0").innerHTML = s0;
        document.querySelector("#s1").innerHTML = s1;
        document.querySelector(".segundos").innerHTML=s;
    }, 200);

})